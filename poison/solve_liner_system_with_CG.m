function x = solve_liner_system_with_CG(A,b,eps)
  ## cf. https://ja.wikipedia.org/wiki/%E5%85%B1%E5%BD%B9%E5%8B%BE%E9%85%8D%E6%B3%95
  x = zeros(size(b));
  n = size(b)(1);
  r = b-A*x;
  p = r;
  for __i=(1:n)
    alpha = (r'*p)/(p'*A*p);
    x = x+alpha*p;
    prev_r = r;
    r=r-alpha*A*p;
    printf("i=%d,diff=%f\n",__i,norm(r));
    fflush(stdout);
    if (norm(r)/norm(b)<eps)
      break;
    end
    beta = (r'*r)/(prev_r'*prev_r);
    p = r+beta*p;
  end
endfunction
