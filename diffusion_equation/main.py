import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

fig = plt.figure()
n = 20
dt = 0.01
ub = 1
dx = ub/n
c = 1
x = np.zeros(n+1)
for i in range(0,n+1):
    if i>0:
        x[i]=x[i-1]+dx
    else:
        pass
u = c*np.ones(n+1)
#u = np.asarray([-y*y+y for y in x.tolist()])
kappa = 0.001
# r = dt*kappa/(dx*dx)
r=0.3
ims = []
for t in range(0,100):
    line, = plt.plot(x,u,"r")
    ims.append([line])
    unext = np.zeros(n+1)
    for i in range(1,n):
        unext[i] = r*u[i+1]+(1-2*r)*u[i]+r*u[i-1]
    unext[n]=c
    u = unext

ani = animation.ArtistAnimation(fig, ims)
ani.save('anim.gif', writer="imagemagick")
plt.show()
