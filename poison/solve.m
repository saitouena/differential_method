N = 100;
dx = 1/N; # もっと小さいほうがいいかも
dy = 1/N;
A = zeros((N-1)*(N-1));
c = -1; # f(x,y)=c
b = c*ones((N-1)*(N-1),1);
u0 = 0; # 境界線上で取る値
for i=1:(N-1)
  for j=1:(N-1)
    k = j+(N-1)*(i-1);
    idx = k;
    A(k,idx) = (-2)*(1/(dx*dx)+1/(dy*dy));
    idx = j+(N-1)*((i+1)-1);
    if (i+1==N) # u(i+1,j)
      b(k) -= u0/(dx*dx);
    else
      A(k,idx) = 1/(dx*dx);
    end
    idx = j+(N-1)*((i-1)-1);
    if (i-1==0) # u(i-1,j)
      b(k) -= u0/(dx*dx);
    else
      A(k,idx) = 1/(dx*dx);
    end
    idx = j+1+(N-1)*(i-1);
    if (j+1==N) # u(i,j+1)
      b(k) -= u0/(dy*dy);
    else
      A(k,idx) = 1/(dy*dy);
    end
    idx = j-1+(N-1)*(i-1);
    if (j-1==0) # u(i,j-1);
      b(k) -= u0/(dy*dy);
    else
      A(k,idx) = 1/(dy*dy);
    end
  end
end
eps = 1e-12;
u = solve_liner_system_with_CG(A,b,eps);
## x = zeros((N+1)*(N+1));
## y = zeros((N+1)*(N+1));
## z = zeros((N+1)*(N+1));
## for i=0:N
##   for j=0:N
##     idx = (j+1)+(N+1)*i;
##     x(idx) = i*dx;
##     y(idx) = j*dy;
##     if (i==0||i==N||j==0||j==N)
##       z(idx) = u0;
##     else
##       z(idx) = u(j+(N-1)*(i-1));
##     end
##   end
## end
## figure
## mesh(x,y,z);
